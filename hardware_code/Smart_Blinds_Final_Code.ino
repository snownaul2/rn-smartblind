
// Full finished code for Smart Blinds

// libraries
#include <FirebaseArduino.h>
#include <ESP8266WiFi.h>
#include <AccelStepper.h>



//Wifi settings
#define WIFI_SSID "AndroidAP"
#define WIFI_PASSWORD "wbvp2428"


//Fireabase Settings 
#define FIREBASE_HOST "smart-blind-ktj-default-rtdb.firebaseio.com"
#define FIREBASE_AUTH "BUpBKppQKd92bYFtTJgIJfUIr8rIDpz3hvit1mFp"


#define AUTOH "devices/pppp-pppp-pppp-pppp/autoMode/height"
#define MANUALH "devices/pppp-pppp-pppp-pppp/manualMode/height"
#define AUTOA "devices/pppp-pppp-pppp-pppp/autoMode/angle"
#define MANUALA "devices/pppp-pppp-pppp-pppp/manualMode/angle"

int i = 0;
int j = 0;
#define stepPin D6 // set step pin to D6
#define dirPin D5  // set direction pin to D5

#define IN1 D1
#define IN2 D2
#define IN3 D3
#define IN4 D4
#define ANALOG_IN A0

// 28byj-48 stepper motor 
AccelStepper stepper(AccelStepper:: FULL4WIRE, IN1, IN3, IN2, IN4);


void setup() {
  Serial.begin(9600);
  delay(100);


  // Connect to Wi-Fi
  Serial.print("Wi-Fi...");
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("Connecting...");
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }


  // connected to wifi
  Serial.println();
  Serial.print("Connected to: ");
  Serial.println(WiFi.localIP());

  // connect to Firebase
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  delay(300);

  
  // Sets the two pins as Outputs
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);

  stepper.setMaxSpeed(600);
  stepper.setAcceleration(1000);
  delay(200);

 Firebase.stream("devices/pppp-pppp-pppp-pppp");
  delay(1000);
}

void loop() {
yield();

  int modeF = Firebase.getInt("devices/pppp-pppp-pppp-pppp/mode"); //getting the mode from firebase
 // yield();
 delay(300);

     int ldr = analogRead(ANALOG_IN);
    int light_read = ldr / 10.23;
    Firebase.setInt("devices/pppp-pppp-pppp-pppp/autoMode/light", light_read);  // 0 - 1023  - will send from 0 - 100
    delay(300);
  // if mode = 0 - manual mode
  if ( modeF == 0) {
    yield();
    // if streaming failed give message
    if (Firebase.failed()) {
      Serial.println("streaming error");
      Serial.println(Firebase.error());
      return;
    }

    // if stream available get the height
    if (Firebase.available()) {
          yield();

      FirebaseObject event2 = Firebase.readEvent();
      int data = event2.getInt("manualMode/height");
      int data2 = event2.getInt("manualMode/angle");
      
      if (data == 0 || data2 == 0) {
        int htM = Firebase.getInt(MANUALH);       // if the height value changes read it and move the motor accordingly
        delay(300);
        movHF(htM);
        //yield();
        int anM = Firebase.getInt(MANUALA);
        delay(300);
        movAF(anM);
       // yield();
      }
      
    }
   // if mode = 1 go to auto mode
  } else if (modeF == 1){
        delay(300);
    if (Firebase.failed()) {
      Serial.println("streaming error");
      Serial.println(Firebase.error());
      return;
    }
    delay(600);

    if (Firebase.available()) {     // if there is a change in the light  value then it will change to the angle value that is set in the database
      int anA = Firebase.getInt(AUTOA);
      delay(300);
      int htA = Firebase.getInt(AUTOH);
      delay(300);
      autoMov(ldr, anA, htA);
    }
    delay(600);
  }
  delay(600);
}

// height move foward
int movHF(int height) {
  movHB(height);
// 10% height
  if (height <= 10 && height >0) {
    if (i < 400) {
    digitalWrite(dirPin, HIGH); // 200 steps - 1 rev foward from height =0
    for ( i = 1; i <= 400; i++) {
      digitalWrite(stepPin, HIGH);
      delay(10);
      digitalWrite(stepPin, LOW);
      delay(10);
      yield();
    }
  //  yield();
    i =401;
    }
}
// 20% height
  if(height <= 20 && height > 10){
    digitalWrite(dirPin, HIGH); // move foward - 400 steps from height =0
    for ( i = 1; i <= 600-j; i++) {
      digitalWrite(stepPin, HIGH);
      delay(10);
      digitalWrite(stepPin, LOW);
      delay(10);
      yield();
    }
 //   yield();
    i=601;
  }
// 30% height
  if(height <= 30 && height > 20){
    digitalWrite(dirPin, HIGH); // move foward - 400 steps from height =0
    for ( i = 1; i <= 800-j; i++) {
      digitalWrite(stepPin, HIGH);
      delay(10);
      digitalWrite(stepPin, LOW);
      delay(10);
      yield();
    }
  //  yield();
    i=801;
  }
// 40% height
  if(height <= 40 && height > 30){
    digitalWrite(dirPin, HIGH); // move foward - 400 steps from height =0
    for ( i = 1; i <= 1000-j; i++) {
      digitalWrite(stepPin, HIGH);
      delay(10);
      digitalWrite(stepPin, LOW);
      delay(10);
      yield();
    }
    yield();
    i=1001;
  }
// 50% height
  if(height <= 50 && height > 40){
    digitalWrite(dirPin, HIGH); // move foward - 400 steps from height =0
    for ( i = 1; i <= 1200-j; i++) {
      digitalWrite(stepPin, HIGH);
      delay(10);
      digitalWrite(stepPin, LOW);
      delay(10);
      yield();
    }
   // yield();
    i=1201;
  }
// 60% height
  if(height <= 60 && height > 50){
    digitalWrite(dirPin, HIGH); // move foward - 400 steps from height =0
    for ( i = 1; i <= 1400-j; i++) {
      digitalWrite(stepPin, HIGH);
      delay(10);
      digitalWrite(stepPin, LOW);
      delay(10);
      yield();
    }
  //  yield();
    i=1401;
  }
// 70% height
  if(height <=70 && height > 60){
    digitalWrite(dirPin, HIGH); // move foward - 400 steps from height =0
    for ( i = 1; i <= 1600-j; i++) {
      digitalWrite(stepPin, HIGH);
      delay(10);
      digitalWrite(stepPin, LOW);
      delay(10);
      yield();
    }
  //  yield();
    i=1601;
  }
// 80% height
  if(height <= 80 && height >70 ){
    digitalWrite(dirPin, HIGH); // move foward - 400 steps from height =0
    for ( i = 1; i <= 1800-j; i++) {
      digitalWrite(stepPin, HIGH);
      delay(10);
      digitalWrite(stepPin, LOW);
      delay(10);
      yield();
    }
   // yield();
    i=1801;
  }
// 90% height
  if(height <= 90 && height > 80){
    digitalWrite(dirPin, HIGH); // move foward - 400 steps from height =0
    for ( i = 1; i <= 2000-j; i++) {
      digitalWrite(stepPin, HIGH);
      delay(10);
      digitalWrite(stepPin, LOW);
      delay(10);
      yield();
    }
//    yield();
    i=2001;
  }
// 100% height
  if(height <= 100 && height > 90){
    digitalWrite(dirPin, HIGH); // move foward - 400 steps from height =0
    for ( i = 1; i <= 2200-j; i++) {
      digitalWrite(stepPin, HIGH);
      delay(10);
      digitalWrite(stepPin, LOW);
      delay(10);
      yield();
    }
//    yield();
    i=2201;
  }

    j =i;
   // Serial.println(j);
}

// height move backward 
int movHB(int h1){
// 0& height - back
if(h1 == 0 && i>0){
      digitalWrite(dirPin, LOW);
     for ( i = 1; i <= j-1 ; i++) {
      digitalWrite(stepPin, HIGH);
      delay(10);
      digitalWrite(stepPin, LOW);
      delay(10);
      yield();
    }
 //   yield();
    i = 0;
  }
// 10% height - back
if((h1<=10 && h1>0)  && i>401){
      digitalWrite(dirPin, LOW);
     for ( i = 1; i <= j-400 ; i++) {
      digitalWrite(stepPin, HIGH);
      delay(10);
      digitalWrite(stepPin, LOW);
      delay(10);
      yield();
    }
 //   yield();
    i = 401;
  }
// 20%
if((h1<=20 && h1>10) && i>601){
      digitalWrite(dirPin, LOW);
     for ( i = 1; i <= j-600 ; i++) {
      digitalWrite(stepPin, HIGH);
      delay(10);
      digitalWrite(stepPin, LOW);
      delay(10);
      yield();
    }
   // yield();
    i = 601;
  }
// 30%
if((h1<=30 && h1>20) && i>801){
      digitalWrite(dirPin, LOW);
     for ( i = 1; i <= j-800 ; i++) {
      digitalWrite(stepPin, HIGH);
      delay(10);
      digitalWrite(stepPin, LOW);
      delay(10);
      yield();
    }
  //  yield();
    i = 801;
  }
// 40%
if((h1<= 40 && h1>30) && i>1001){
      digitalWrite(dirPin, LOW);
     for ( i = 1; i <= j-1000 ; i++) {
      digitalWrite(stepPin, HIGH);
      delay(10);
      digitalWrite(stepPin, LOW);
      delay(10);
      yield();
    }
 //   yield();
    i = 1001;
  }
// 50%
if((h1<= 50 && h1>40) && i>1201){
      digitalWrite(dirPin, LOW);
     for ( i = 1; i <= j-1200 ; i++) {
      digitalWrite(stepPin, HIGH);
      delay(10);
      digitalWrite(stepPin, LOW);
      delay(10);
      yield();
    }
   // yield();
    i = 1201;
  }
// 60%
if((h1<= 60 && h1>50) && i>1401){
      digitalWrite(dirPin, LOW);
     for ( i = 1; i <= j-1400 ; i++) {
      digitalWrite(stepPin, HIGH);
      delay(10);
      digitalWrite(stepPin, LOW);
      delay(10);
      yield();
    }
 //   yield();
    i = 1401;
  }
// 70%
if((h1<= 70 && h1>60) && i>1601){
      digitalWrite(dirPin, LOW);
     for ( i = 1; i <= j-1600 ; i++) {
      digitalWrite(stepPin, HIGH);
      delay(10);
      digitalWrite(stepPin, LOW);
      delay(10);
      yield();
    }
  //  yield();
    i = 1601;
  }
// 80%
if((h1<= 80 && h1>70) && i>1801){
      digitalWrite(dirPin, LOW);
     for ( i = 1; i <= j-1800 ; i++) {
      digitalWrite(stepPin, HIGH);
      delay(10);
      digitalWrite(stepPin, LOW);
      delay(10);
      yield();
    }
   // yield();
    i = 1801;
  }
// 90%
if((h1<= 90 && h1>80) && i>2001){
      digitalWrite(dirPin, LOW);
     for ( i = 1; i <= j-2000 ; i++) {
      digitalWrite(stepPin, HIGH);
      delay(10);
      digitalWrite(stepPin, LOW);
      delay(10);
      yield();
    }
  //  yield();
    i = 2001;
  }
}

// move angle foward
int movAF(int angle){
  movAB(angle);
  yield();
  if (angle <= 10 && angle >0) {
    while (stepper.currentPosition() != 102 ) {
      stepper.setSpeed(300);
      stepper.runSpeed();
      yield();
    }
  }

  if (angle <= 20 && angle >10 ) {
    while (stepper.currentPosition() != 204 ) {
      stepper.setSpeed(300);
      stepper.runSpeed();
      yield();
    }
  }

  if (angle <= 30 && angle >20) {
    while (stepper.currentPosition() != 306 ) {
      stepper.setSpeed(300);
      stepper.runSpeed();
      yield();
    }
  }

  if (angle <= 40 && angle >30) {
    while (stepper.currentPosition() != 408 ) {
      stepper.setSpeed(300);
      stepper.runSpeed();
      yield();
    }
  }

  if (angle <= 50 && angle >40) {
    while (stepper.currentPosition() != 510 ) {
      stepper.setSpeed(300);
      stepper.runSpeed();
      yield();
    }
  }

  if (angle <= 60 && angle >50) {
    while (stepper.currentPosition() != 612 ) {
      stepper.setSpeed(300);
      stepper.runSpeed();
      yield();
    }
  }

  if (angle <= 70 && angle >60) {
    while (stepper.currentPosition() != 714 ) {
      stepper.setSpeed(300);
      stepper.runSpeed();
      yield();
    }
  }

  if (angle <= 80 && angle >70) {
    while (stepper.currentPosition() != 816 ) {
      stepper.setSpeed(300);
      stepper.runSpeed();
      yield();
    }
  }

  if (angle <=90 && angle >80) {
    while (stepper.currentPosition() != 918 ) {
      stepper.setSpeed(300);
      stepper.runSpeed();
      yield();
    }
  }

  if (angle <= 100 && angle >90) {
    while (stepper.currentPosition() != 1020 ) {
      stepper.setSpeed(300);
      stepper.runSpeed();
      yield();

    }
  }
}
// move angle back
int movAB(int ang){
  yield();

  if (ang == 0 && stepper.currentPosition() > 0) {
    while (stepper.currentPosition() != 0 ) {
      stepper.setSpeed(-300);
      stepper.runSpeed();
      yield();
    }
  }

  if (stepper.currentPosition() > 102 && (ang <=10 && ang>0)) {
    while (stepper.currentPosition() != 102 ) {
      stepper.setSpeed(-300);
      stepper.runSpeed();
      yield();
    }
  }

  if (stepper.currentPosition() > 204 && (ang <=20 && ang>10)) {
    while (stepper.currentPosition() != 204 ) {
      stepper.setSpeed(-300);
      stepper.runSpeed();
      yield();
    }
  }

  if (stepper.currentPosition() > 306 && (ang <=30 && ang>20)) {
    while (stepper.currentPosition() != 306 ) {
      stepper.setSpeed(-300);
      stepper.runSpeed();
      yield();
    }
  }

  if (stepper.currentPosition() > 408 && (ang <=40 && ang>30)) {
    while (stepper.currentPosition() != 408 ) {
      stepper.setSpeed(-300);
      stepper.runSpeed();
      yield();
    }
  }

  if (stepper.currentPosition() > 510 && (ang <=50 && ang>40)) {
    while (stepper.currentPosition() != 510 ) {
      stepper.setSpeed(-300);
      stepper.runSpeed();
      yield();
    }
  }

  if (stepper.currentPosition() > 612  && (ang <=60 && ang>50)) {
    while (stepper.currentPosition() != 612 ) {
      stepper.setSpeed(-300);
      stepper.runSpeed();
      yield();
    }
  }

  if (stepper.currentPosition() > 714  && (ang <=70 && ang>60)) {
    while (stepper.currentPosition() != 714 ) {
      stepper.setSpeed(-300);
      stepper.runSpeed();
      yield();
    }
  }

  if (stepper.currentPosition() > 816  && (ang <=80 && ang>70)) {
    while (stepper.currentPosition() != 816 ) {
      stepper.setSpeed(-300);
      stepper.runSpeed();
      yield();
    }
  }

  if (stepper.currentPosition() > 918  && (ang <=90 && ang>80)) {
    while (stepper.currentPosition() != 918 ) {
      stepper.setSpeed(-300);
      stepper.runSpeed();
      yield();
    }
  }
}

////////////// AUTO MOVE//////////////////////////
int autoMov(int light, int a1, int h1) {

  // 10% light
  if (light <= 102) {

    movHF(h1);
    movAF(a1);
    
  }

  // 20%
  if (light > 102 && light <= 204) {
    movHF(h1);
    movAF(a1);

  }
  //30%
  if (light > 204 && light <= 306) {
    movHF(h1);
    movAF(a1);

  }
  //40%
  if (light > 306 && light <= 408) {
     movHF(h1);
    movAF(a1);

  }
  //50%
  if (light > 408 && light <= 510) {
    movHF(h1);
    movAF(a1);


  }
  //60%
  if (light > 510 && light <= 612) {
    movHF(h1);
    movAF(a1);

  }
  //70%
  if (light > 612 && light <= 714) {
    movHF(h1);
    movAF(a1);

  }
  //80%
  if (light > 714 && light <= 816) {
    movHF(h1);
    movAF(a1);

  }
  //90%
  if (light > 816 && light <= 918) {
    movHF(h1);
    movAF(a1);

  }
  //100%
  if (light > 918 && light <= 1020) {
    movHF(h1);
    movAF(a1);
  }
}
