import React from 'react';
import Routes from "./src/Routes";
import { SafeAreaView } from 'react-native';
import { Provider } from 'react-redux';
import { Text } from "react-native";
import { store } from './src/redux/store';

//import { firebaseInstance } from './src/firebase';
import { FirebaseApp } from 'firebase/app';
import { initializeApp } from 'firebase/app';

export const App = () => {
  return (
    <Provider store={store}>
      <SafeAreaView style={{ flex: 1, }}>
        <Routes></Routes>
      </SafeAreaView>
    </Provider>
  );
}
export default App;