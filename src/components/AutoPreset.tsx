import React from 'react';
import { View, Text, StyleSheet, Touchable, Slider } from 'react-native';
// import Slider from '@react-native-community/slider';
import LightSlider from './LightSlider'
import MyHeader from '../ui/MyHeader';
import ColorBox from '../ui/ColorBox';
import MainTextInput from '../ui/MainTextInput';
import MyButton from '../ui/MyButton';
import ManualMode from './ManualMode';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { startAfter } from 'firebase/database/dist/database';

import firebaseService from '../firebase';
import AutoMode from './AutoMode';
import ManualSlider from '../ui/ManualSlider';
import CustomMarker from '../ui/CustomMarker';
import CustomLabel from '../ui/CustomLable';
import AutoPresetItem from '../ui/AutoPresetItem';

export default class AutoPreset extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            add: this.props.route.params.add,
            device_code: this.props.route.params.device_code,
            preset_code: this.props.route.params.preset_code,
            height: this.props.route.params.height ? this.props.route.params.height : 0,
            angle: this.props.route.params.angle ? this.props.route.params.angle : 0,
            light: this.props.route.params.light ? this.props.route.params.light : 0,
            on: this.props.route.params.on
        }
    }



    heightChanged = (h) => {
        h = parseInt(h);
        this.setState({
            height: h
        })

    }

    angleChanged = (a) => {
        a = parseInt(a)
        this.setState({
            angle: a
        })

    }

    lightChanged = (a, b) => {
        this.setState({
            light: a[0],
        })
    }

    onSave = () => {
        if (this.state.add) {
            firebaseService.savePreset(null, this.state.device_code, this.state.height, this.state.angle, this.state.light);

            this.props.navigation.goBack(null)
        } else {
            firebaseService.updatePreset(null, this.state.device_code, this.state.height, this.state.angle, this.state.light, this.state.on, this.state.preset_code)
            this.props.navigation.goBack(null)
        }

    }


    render() {

        return (
            <View style={styles.outer_box}>
                <MyHeader title='Setting'
                    back={true}
                    color={this.state.color}
                    save={true}
                    onSave={this.onSave}
                    deviceCode={this.state.device_code}
                    navigation={this.props.navigation} />

                <View style={styles.light_container}>
                    <View style={styles.light_box}>
                        <Text style={styles.light_title}>
                            Light
                        </Text>
                        <View style={styles.prc_box}>
                            <Text style={styles.num}>{this.state.light}</Text>
                            <Text style={styles.prc}>%</Text>
                        </View>
                    </View>

                    <LightSlider
                        selectedStyle={{
                            backgroundColor: '#FFC424',
                        }}
                        unselectedStyle={{
                            backgroundColor: '#F1F1F5',
                        }}
                        values={[this.state.light]}
                        containerStyle={{
                            height: 40,
                        }}
                        trackStyle={{
                            height: 5,
                            backgroundColor: 'red',
                        }}
                        touchDimensions={{
                            height: 20,
                            width: 20,
                            borderRadius: 20,
                            slipDisplacement: 40,
                        }}
                        customMarker={CustomMarker}
                        customLabel={CustomLabel}
                        sliderLength={280}
                        onValuesChangeFinish={this.lightChanged}
                    />
                </View>

                <View style={styles.contents}>

                    <View style={styles.slider_box}>
                        <ManualSlider
                            maximumValue={100}
                            minimumValue={0}
                            value={this.state.height}
                            percentage={(percent) => this.heightChanged(percent)}
                        />
                        <Text style={styles.s_title}>
                            Height
                        </Text>
                        <View style={styles.prc_box}>
                            <Text style={styles.num}>{this.state.height}</Text>
                            <Text style={styles.prc}>%</Text>
                        </View>
                    </View>
                    <View style={styles.slider_box}>
                        <ManualSlider
                            maximumValue={100}
                            minimumValue={0}
                            value={this.state.angle}
                            percentage={(percent) => this.angleChanged(percent)}
                        />
                        <Text style={styles.s_title}>
                            Angle
                        </Text>
                        <View style={styles.prc_box}>
                            <Text style={styles.num}>{this.state.angle}</Text>
                            <Text style={styles.prc}>%</Text>
                        </View>
                    </View>
                </View>


            </View>
        )
    }

}

const styles = StyleSheet.create({
    contents: {
        padding: 24,
        backgroundColor: '#ffffff',
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 50,
    }, outer_box: {
        backgroundColor: '#ffffff',
        flex: 1,
    }, slider_box: {
        margin: 30,
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    }, s_title: {
        fontSize: 14,
        color: '#999999',
        marginTop: 30,
    }, prc_box: {
        flexDirection: 'row',
        marginTop: 17,
    }, num: {
        fontSize: 36,
        color: '#191919',
        fontWeight: 'bold'
    }, prc: {
        fontSize: 20,
        color: '#191919',
        marginTop: 17,
        marginLeft: 2,
    }, light_box: {
        flexDirection: 'row'
    }, light_title: {
        fontSize: 14,
        color: '#999999',
        marginTop: 30,
        marginRight: 20,
    }, light_container: {
        marginTop: 50,
        alignItems: 'center',
        alignItems: 'center'
    }
})