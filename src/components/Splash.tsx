import React from 'react';
import { View, Text, Button } from 'react-native';

export default class Splash extends React.Component {

    constructor(props: {} | Readonly<{}>) {
        super(props);

        this.autoSignIn = this.autoSignIn.bind(this);
        this.movePage = this.movePage.bind(this);
    }

    autoSignIn() {
        //TODO : autoSignIn

        //Success
        //this.movePage('Devices');

        //Failed
        this.movePage('SignIn');
    }

    movePage(page) {
        this.props.navigation.navigate(page);
    }

    componentDidMount() {
        setTimeout(() => {
            this.autoSignIn();
        }, 2000);
    }

    render() {
        return (
            <View>
                <Text>
                    Splash
                </Text>

            </View>
        )
    }
}