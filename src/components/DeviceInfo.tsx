import React from 'react';
import { View, Text, StyleSheet, Touchable } from 'react-native';
import MyHeader from '../ui/MyHeader';
import ColorBox from '../ui/ColorBox';
import MainTextInput from '../ui/MainTextInput';
import MyButton from '../ui/MyButton';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { startAfter } from 'firebase/database/dist/database';

import firebaseService from '../firebase';

export default class DeviceInfo extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            color: this.props.route.params.color,
            add: this.props.route.params.add,
            device_code: this.props.route.params.device_code,
            device_name: "",
            device_name_war: false,
            device_name_war_msg: "",
            register_valid: false,
            reg_date: "",
            email: this.props.route.params.email,
        }
    }

    componentDidMount() {
        if (this.state.add == false) {
            firebaseService.deviceInfo(this.deviceInfo, this.state.device_code);
        }
    }

    deviceInfo = (info) => {
        console.log(info);
        let data = info.val();
        if (data == null || data.regDate == null) return;
        console.log(data.regDate)

        this.setState({
            device_name: data.deviceName,
            color: data.color,
            reg_date: data.regDate
        })
    }

    registerDevice = () => {

        console.log(this.state.register_valid);
        if (!this.state.register_valid) {
            return;
        }

        //api 호출
        var dCode = this.state.device_code;
        var dName = this.state.device_name;
        var dColor = this.state.color;

        firebaseService.registerDevice(dCode, dName, dColor);

        // 성공 시
        this.props.navigation.goBack(null)

    }

    updateDevice = () => {
        if (!this.state.register_valid) {
            return;
        }

        var dCode = this.state.device_code;
        var dName = this.state.device_name;
        var dColor = this.state.color;

        console.log(dCode, dName, dColor)

        firebaseService.editDeviceInfo(this.state.device_code, this.state.device_name, this.state.color);

        // 성공 시
        this.props.navigation.goBack(null)
    }

    colorChange = (color) => {
        var registerValid = false;
        if (!this.state.add && this.state.device_name.length > 0) {
            registerValid = true;
        } else if (this.state.device_code && this.state.device_code.length == 19 && name.length > 0) {
            registerValid = true;
        }
        this.setState({
            color: color,
            register_valid: registerValid,
        })
    }

    onChangeDeviceName = (name) => {
        var registerValid = false;
        if (!this.state.add && name.length > 0) {
            registerValid = true;
        } else if (this.state.device_code && this.state.device_code.length == 19 && name.length > 0) {
            registerValid = true;
        }
        this.setState({
            device_name: name,
            register_valid: registerValid,
        })
    }

    onChangeDeviceCode = (code) => {
        var reg = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi
        code = code.replace(reg, "");
        var regExp = /^[a-zA-Z0-9-]*$/;
        var isValid = regExp.test(code);
        var deviceCode = ''
        var registerValid = false;
        if (isValid) {

            if (code.length < 5) {
                deviceCode = code
            } else if (code.length < 9) {
                deviceCode += code.substr(0, 4);
                deviceCode += '-';
                deviceCode += code.substr(4);
            } else if (code.length < 13) {
                deviceCode += code.substr(0, 4);
                deviceCode += '-';
                deviceCode += code.substr(4, 4);
                deviceCode += '-';
                deviceCode += code.substr(8);
            } else if (code.length <= 16) {
                deviceCode += code.substr(0, 4);
                deviceCode += '-';
                deviceCode += code.substr(4, 4);
                deviceCode += '-';
                deviceCode += code.substr(8, 4);
                deviceCode += '-';
                deviceCode += code.substr(12);
            }

            if (deviceCode.length == 19 && this.state.device_name.length > 0) {
                registerValid = true;
            }

            this.setState({
                device_code: deviceCode,
                register_valid: registerValid
            })
        }

    }

    render() {
        return (
            <View style={styles.outer_box}>
                <MyHeader title="Device Info"
                    back={true}
                    navigation={this.props.navigation} />
                <View style={styles.contents}>
                    <ColorBox
                        onPress={this.colorChange}
                        color={this.state.color}
                    />

                    <MainTextInput
                        placeholder="Device Name"
                        maxLength={16}
                        value={this.state.device_name}
                        onChangeText={this.onChangeDeviceName}
                    />
                    {this.state.add &&
                        <MainTextInput
                            placeholder="Device Code"
                            maxLength={19}
                            value={this.state.device_code}
                            onChangeText={this.onChangeDeviceCode} />
                    }

                    {!this.state.add &&
                        <View>
                            <View>
                                <Text style={styles.stitle}>Device Code</Text>
                                <Text style={styles.info}>{this.state.device_code}</Text>
                            </View>
                            <View>
                                <Text style={styles.stitle}>Registered Date/Time</Text>
                                <Text style={styles.info}>{this.state.reg_date}</Text>
                            </View>
                            <View>
                                <Text style={styles.stitle}>Registered by</Text>
                                <Text style={styles.info}>{this.state.email}</Text>
                            </View>

                            <TouchableOpacity>
                                <Text style={styles.reset}>Reset to default setting</Text>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => {

                                this.props.navigation.goBack(null)
                                this.props.navigation.goBack(null)
                                // this.props.navigation.pop();
                                firebaseService.removeDevcie(this.state.device_code);
                                // this.props.navigation.navigate('Devices', {
                                //     email: this.state.email
                                // })
                            }}>
                                <Text style={styles.remove}>Remove device</Text>
                            </TouchableOpacity>
                        </View>
                    }

                    {this.state.add &&
                        <MyButton
                            color={this.state.register_valid ? "#FFC424" : "#FFE49B"}
                            titleColor="#ffffff"
                            onPress={() => this.registerDevice()}
                            disabled={!this.state.register_valid}
                            title="REGISTER DEVICE" />
                    }

                    {!this.state.add &&
                        <MyButton
                            color={this.state.register_valid ? "#FFC424" : "#FFE49B"}
                            titleColor="#ffffff"
                            onPress={() => this.updateDevice()}
                            disabled={this.state.device_name == undefined || this.state.device_name.length == 0}
                            title="UPDATE DEVICE" />
                    }

                </View>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    contents: {
        padding: 24,
        backgroundColor: '#ffffff',
        flex: 1,
        // justifyContent: 'center'
    }, outer_box: {
        backgroundColor: '#ffffff',
        flex: 1,
    }, stitle: {
        marginTop: 18,
        color: '#999999',
        fontSize: 12,
    }, info: {
        color: '#191919',
        fontSize: 14,
        marginTop: 5,
    }, reset: {
        color: '#999999',
        fontSize: 14,
        marginTop: 18,
        textDecorationLine: 'underline',
    }, remove: {
        color: '#E62C18',
        fontSize: 14,
        marginTop: 18,
        textDecorationLine: 'underline',
    }
})