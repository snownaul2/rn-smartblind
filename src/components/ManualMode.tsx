import React from 'react';
import { View, Text, StyleSheet, Touchable } from 'react-native';
import MyHeader from '../ui/MyHeader';
import { TouchableOpacity } from 'react-native-gesture-handler';
import ManualSlider from '../ui/ManualSlider';


export default class ManualMode extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            device_name: "",
            device_name_war: false,
            device_name_war_msg: "",
            device_mode: 0,
        }
    }

    render() {
        return (
            <View style={styles.outer_box}>
                <View style={styles.contents}>

                    <View style={styles.slider_box}>
                        <ManualSlider
                            maximumValue={100}
                            minimumValue={0}
                            value={this.props.height}
                            percentage={(percent) => this.props.heightChanged(percent)}
                        />
                        <Text style={styles.s_title}>
                            Height
                        </Text>
                        <View style={styles.prc_box}>
                            <Text style={styles.num}>{this.props.height}</Text>
                            <Text style={styles.prc}>%</Text>
                        </View>
                    </View>
                    <View style={styles.slider_box}>
                        <ManualSlider
                            maximumValue={100}
                            minimumValue={0}
                            value={this.props.angle}
                            percentage={(percent) => this.props.angleChanged(percent)}
                        />
                        <Text style={styles.s_title}>
                            Angle
                        </Text>
                        <View style={styles.prc_box}>
                            <Text style={styles.num}>{this.props.angle}</Text>
                            <Text style={styles.prc}>%</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    contents: {
        padding: 24,
        backgroundColor: '#ffffff',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    }, outer_box: {
        backgroundColor: '#ffffff',
        flex: 1,
    }, slider_box: {
        margin: 30,
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    }, s_title: {
        fontSize: 14,
        color: '#999999',
        marginTop: 30,
    }, prc_box: {
        flexDirection: 'row',
        marginTop: 17,
    }, num: {
        fontSize: 36,
        color: '#191919',
        fontWeight: 'bold'
    }, prc: {
        fontSize: 20,
        color: '#191919',
        marginTop: 17,
        marginLeft: 2,
    }
})