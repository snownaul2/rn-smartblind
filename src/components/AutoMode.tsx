import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import MyHeader from '../ui/MyHeader';
import { TouchableOpacity } from 'react-native-gesture-handler';
import AutoSlider from '../ui/AutoSlider';
import AutoPresetItem from '../ui/AutoPresetItem';
import firebaseService from '../firebase';
import { SwipeListView } from 'react-native-swipe-list-view';


export default class AutoMode extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            // color: this.props.route.params.color,
            // add: this.props.route.params.add,
            device_code: this.props.device_code,
            device_name: "SAMPLE DEVICE NAME",
            device_name_war: false,
            device_name_war_msg: "",
            device_mode: 0,
            list: [],
        }
    }

    componentDidMount() {
        firebaseService.presetList(this.onListChanged, this.state.device_code);
    }

    onListChanged = (items) => {
        console.log("LIST")
        console.log(items);
        let newList = [];
        items.forEach((item) => {
            let data = item.val();
            console.log(data);
            newList.unshift({
                preset_code: item.key,
                height: parseInt(data.height),
                angle: parseInt(data.angle),
                light: parseInt(data.preLight),
                active: parseInt(data.active),
                on: parseInt(data.button),
            })
        })


        console.log(newList)
        this.setState({
            list: newList
        })

    }

    render() {
        return (
            <View style={styles.outer_box}>
                <View style={styles.contents}>
                    <AutoSlider
                        value={this.props.light}
                    />
                    <View style={styles.light_box}>
                        <Text style={styles.s_title}>
                            Light
                        </Text>
                        <View style={[styles.prc_box, styles.light_box]}>
                            <Text style={styles.num}>{this.props.light}</Text>
                            <Text style={styles.prc}>%</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.ha_box}>
                    <View style={styles.ha_inner_box}>
                        <Text style={styles.s_title}>
                            Height
                        </Text>
                        <View style={styles.prc_box}>
                            <Text style={styles.num}>{this.props.height}</Text>
                            <Text style={styles.prc}>%</Text>
                        </View>
                    </View>
                    <View style={styles.ha_inner_box}>
                        <Text style={styles.s_title}>
                            Angle
                        </Text>
                        <View style={styles.prc_box}>
                            <Text style={styles.num}>{this.props.angle}</Text>
                            <Text style={styles.prc}>%</Text>
                        </View>
                    </View>
                </View>

                <View style={styles.divide_line} />
                <View style={styles.down_box}>
                    <TouchableOpacity
                        style={styles.add_box}
                        onPress={() => this.props.onPreset(true)}
                    >
                        <Text style={styles.add_text}>Add</Text>
                        <Image
                            style={styles.add_icon}
                            source={require('../../assets/img/add2.png')} />
                    </TouchableOpacity>
                </View>

                <View style={styles.list_box}>

                    {this.state.list &&

                        <SwipeListView
                            data={this.state.list}
                            renderItem={(row, data) => (
                                < AutoPresetItem style={styles.item}
                                    key={row.item.preset_code}
                                    preset_code={row.item.preset_code}
                                    active={row.item.preset_code == this.props.active ? 1 : 0}
                                    on={row.item.on}
                                    light={row.item.light}
                                    height={row.item.height}
                                    angle={row.item.angle}
                                    menu={() => {
                                        this.props.navigation.navigate('AutoPreset', {
                                            preset_code: row.item.preset_code,
                                            add: false,
                                            device_code: this.state.device_code,
                                            height: row.item.height,
                                            angle: row.item.angle,
                                            light: row.item.light,
                                            on: row.item.on,
                                        })
                                    }}
                                    onOffChanged={(on) => {
                                        firebaseService.updatePreset(null, this.state.device_code, row.item.height, row.item.angle, row.item.light, on == 0 ? 1 : 0, row.item.preset_code)
                                    }}
                                />
                            )}
                            renderHiddenItem={(row, rowMap) => (
                                <TouchableOpacity
                                    style={styles.delete_box}
                                    onPress={() => {
                                        firebaseService.removePreset(this.state.device_code, row.item.preset_code);

                                    }}>
                                    <Image
                                        style={styles.del}
                                        source={require('../../assets/img/del.png')} />
                                </TouchableOpacity>
                            )}
                            leftOpenValue={78}
                            automaticallyAdjustContentInsets={true}
                        >

                        </SwipeListView>
                    }
                    {/* {this.state.list && this.state.list.map(
                        (row, i) => {

                            console.log("activekey : " + this.props.active)
                            console.log('curkey : ' + row.preset_code + " ,active: " + (row.preset_code == this.props.active ? 1 : 0))
                            return (
                                <AutoPresetItem style={styles.item}
                                    key={i}
                                    preset_code={row.preset_code}
                                    active={row.preset_code == this.props.active ? 1 : 0}
                                    on={row.on}
                                    light={row.light}
                                    height={row.height}
                                    angle={row.angle}
                                    menu={() => {
                                        this.props.navigation.navigate('AutoPreset', {
                                            preset_code: row.preset_code,
                                            add: false,
                                            device_code: this.state.device_code,
                                            height: row.height,
                                            angle: row.angle,
                                            light: row.light,
                                            on: row.on,
                                        })
                                    }}
                                    onOffChanged={(on) => {
                                        firebaseService.updatePreset(null, this.state.device_code, row.height, row.angle, row.light, on == 0 ? 1 : 0, row.preset_code)
                                    }}
                                />
                            )
                        }
                    )} */}

                </View>
            </View >
        )
    }

}

const styles = StyleSheet.create({
    contents: {
        padding: 24,
        backgroundColor: '#ffffff',
        // flex: 1,
        marginTop: 60,
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: 40,
    }, outer_box: {
        backgroundColor: '#ffffff',
        flex: 1,
        flexDirection: 'column'
    }, light_box: {
        width: 100,
    }, prc_box: {
        flexDirection: 'row',
        marginTop: 17,
    }, num: {
        fontSize: 36,
        color: '#191919',
        fontWeight: 'bold',
    }, prc: {
        fontSize: 20,
        color: '#191919',
        marginTop: 17,
        marginLeft: 2,
    }, s_title: {
        fontSize: 14,
        color: '#999999',
        marginTop: 30,
    }, light_box: {
        marginTop: 25,
    }, ha_box: {
        flexDirection: 'row',
        justifyContent: 'center'
    }, ha_inner_box: {
        marginHorizontal: 40,
    }, divide_line: {
        backgroundColor: '#F8F8FA',
        height: 8,
        width: '100%',
        flexDirection: 'row',
        marginTop: 90,
    }, down_box: {
        padding: 24,
    }, add_box: {
        flexDirection: 'row',
        width: 71,
        height: 33,
        alignContent: 'center',
        borderRadius: 9,
        borderWidth: 0.5,
        borderColor: '#DBDBDB',
        alignSelf: 'flex-end',
        justifyContent: 'center',
        alignItems: 'center',
    }, add_text: {
        color: '#767676',
        fontSize: 14,
    }, add_icon: {
        width: 14,
        height: 14,
    }, list_box: {
        paddingTop: 24,
        paddingBottom: 24,
        flexDirection: 'column',
        flexWrap: 'wrap',
        paddingHorizontal: 12,
        // backgroundColor: 'purple',
    }, delete_box: {
        backgroundColor: '#F8F8FA',
        width: 78,
        height: 63,
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    }, del: {
        width: 28,
        height: 28,
        alignSelf: 'center',

    },
})