import React from 'react';
import { View, Text, Button, Image, StyleSheet, SafeAreaView } from 'react-native';
import MainTextInput from '../ui/MainTextInput'
import MyButton from '../ui/MyButton'
import { TouchableOpacity } from "react-native-gesture-handler";
import MyTextButton from '../ui/MyTextButton';

import firebaseService from '../firebase';

export default class SignIn extends React.Component {

    autoSignIn = true

    constructor(props) {
        super(props);
        this.state = {
            email: this.autoSignIn ? "ubstudent2022@gmail.com" : "",
            psw: this.autoSignIn ? "Ubstudent2022" : "",
            email_war: false,
            psw_war: false,
            email_war_msg: "",
            psw_war_msg: "",
        }
        this.onChangeEmail.bind(this);
    }

    componentDidMount() {
        if (this.autoSignIn)
            this.onSignIn();
    }

    onChangeEmail = (email) => {
        var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i
        var isValid = regExp.test(email);
        this.setState({
            email: email,
            email_war: !isValid,
            email_war_msg: "Please enter a valid email address"
        })
    }

    onChangePassword = (psw) => {
        this.setState({
            psw: psw,
            psw_war: false
        })
    }

    onSignIn = () => {
        //api 호출
        var emaill = this.state.email;
        var psww = this.state.psw;

        firebaseService.signInUser(emaill, psww, this.onSuccess, this.onError);

    }

    onSuccess = () => {
        var email = this.state.email
        this.setState({

            email: "",
            psw: "",
            email_war: false,
            psw_war: false,
            email_war_msg: "",
            psw_war_msg: "",

        })
        this.props.navigation.navigate('Devices', {
            email: email
        })
    }

    onError = (errorMessage) => {
        if (errorMessage == undefined) {
            errorMessage = "An error has occurred please try again";
        }
        console.log(errorMessage);
        this.setState({
            email_war: true,
            email_war_msg: errorMessage
        })
    }

    render() {
        return (
            <View style={styles.contents}>
                <View style={{ flex: 0.8 }} />

                <Image
                    style={styles.logo}
                    source={require('../../assets/img/logo_w.png')} />

                <MainTextInput
                    placeholder="Email Address"
                    onChangeText={this.onChangeEmail}
                    keyboardType="email-address"
                    warning={this.state.email_war}
                    maxLength={320}
                    warning_msg={this.state.email_war_msg}
                    value={this.state.email} />
                <MainTextInput
                    placeholder="Password"
                    value={this.state.psw}
                    maxLength={16}
                    secureTextEntry={true}
                    warning={this.state.psw_war}
                    secureTextEntry={true}
                    warning_msg={this.state.psw_war_msg}
                    onChangeText={this.onChangePassword} />
                <MyButton
                    title="SIGN IN"
                    color="#FFC424"
                    titleColor="#ffffff"
                    onPress={this.onSignIn} />

                <View style={{ flex: 1 }} />

                <SafeAreaView
                    style={{
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                        justifyContent: "center",
                        alignContent: "center",
                        alignItems: "center",
                        marginBottom: 20
                    }}>
                    <Text
                        style={{
                            fontSize: 12,
                            color: "#767676",

                        }}>Don't have an account? </Text>
                    <TouchableOpacity
                        style={[
                            styles.text_button,
                        ]}
                        onPress={() => {
                            this.setState({

                                email: "",
                                psw: "",
                                email_war: false,
                                psw_war: false,
                                email_war_msg: "",
                                psw_war_msg: "",

                            })
                            this.props.navigation.navigate('SignUp')
                        }}
                    >
                        <Text
                            style={[
                                styles.title,
                            ]}>Sign up</Text>
                    </TouchableOpacity>
                </SafeAreaView>


            </View>
        )
    }
}

const styles = StyleSheet.create({
    contents: {
        padding: 24,
        backgroundColor: '#ffffff',
        flex: 1,
        justifyContent: 'center'
    },
    logo: {
        width: 134,
        height: 134,
        resizeMode: "contain",
        marginBottom: 105,
        alignSelf: "center"
    },
    text_button: {
        // justifyContent: 'center',
        alignItems: 'center',
        fontWeight: "bold",
        fontSize: 36,

    },
    title: {
        fontSize: 14,
        // alignSelf: "center",
        color: "#FFC424"
    }
})