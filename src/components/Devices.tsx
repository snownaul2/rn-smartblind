import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import MyHeader from '../ui/MyHeader';
import MyDeviceItem from '../ui/MyDeviceItem';

import firebaseService from '../firebase';

export default class Devices extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            device_list: [],
        }
        this.onDataChange = this.onDataChange.bind(this);
    }

    componentDidMount() {
        firebaseService.deviceList(this.onDataChange);
    }

    onDataChange(items) {
        let newList = [];

        items.forEach((item) => {

            let data = item.val();
            console.log(data)
            newList.push({
                deviceCode: item.key,
                deviceName: data.deviceName,
                color: data.color,
                mode: 1,
            })
        })

        this.setState({
            device_list: newList
        })
    }

    render() {
        return (
            <View style={styles.outer_box}>
                <MyHeader title="Devices"
                    back={true}
                    setting={true}
                    navigation={this.props.navigation} />
                <View style={styles.contents}>
                    {this.state.device_list && this.state.device_list.map(
                        (row, i) => {
                            return (
                                <MyDeviceItem style={styles.item}
                                    key={i}
                                    add={false}
                                    deviceCode={row.deviceCode}
                                    deviceName={row.deviceName}
                                    color={row.color}
                                    onPress={() => {
                                        console.log('clicked')
                                        this.props.navigation.navigate('DeviceControl', {
                                            color: row.color,
                                            add: false,
                                            device_code: row.deviceCode,
                                            email: this.props.route.params.email,
                                        })
                                    }}
                                />
                            )
                        }
                    )}
                    <MyDeviceItem style={styles.item}
                        add={true}
                        onPress={() => {
                            this.props.navigation.navigate('DeviceInfo', {
                                color: 1,
                                add: true
                            })
                        }}
                    />
                </View>

            </View>
        )
    }

}

const styles = StyleSheet.create({
    contents: {
        paddingTop: 24,
        paddingBottom: 24,
        backgroundColor: '#F8F8FA',
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingHorizontal: 12,

    }, item: {
        width: '48%',
    }, outer_box: {

        backgroundColor: '#F8F8FA',
        flex: 1,
    },
})