import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import MyHeader from '../../ui/MyHeader';
import MyMenuItem from '../../ui/MyMenuItem';

import firebaseService from '../../firebase';

export default class Settings extends React.Component {

    signOutCall = () => {
        //signOutCallEndpoint
        firebaseService.signOutUser(() => {
            //Success
            this.props.navigation.navigate('SignIn')
        });


    }

    render() {
        return (
            <View style={styles.outer_box}>
                <MyHeader title="Settings"
                    back={true}
                    emptyRight={true}
                    navigation={this.props.navigation} />
                <View>
                    <MyMenuItem
                        title="Sign Out"
                        onPress={this.signOutCall}
                    />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    outer_box: {
        backgroundColor: '#ffffff',
        flex: 1,
    },
})