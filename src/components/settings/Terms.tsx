import React from 'react';
import { View, Text, Button } from 'react-native';
import MyHeader from '../../ui/MyHeader';

export default class Terms extends React.Component {
    render() {
        return (
            <View>
                <MyHeader title="Terms of Agreement"
                    back={true}
                    navigation={this.props.navigation} />
                <Text>
                    Terms
                </Text>
            </View>
        )
    }
}