import React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import MyHeader from '../ui/MyHeader';
import MainTextInput from '../ui/MainTextInput';
import MyButton from '../ui/MyButton';

import firebaseService from '../firebase'

export default class SignUp extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            psw1: "",
            psw2: "",
            email_war: false,
            psw1_war: false,
            psw1_war_msg: "",
            psw2_war: false,
            email_war_msg: "",
            radio: false,
        }
        this.onChangeEmail.bind(this);
        this.signUpCall.bind(this);
    }

    onChangeEmail = (email) => {
        var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i
        var isValid = regExp.test(email);
        this.setState({
            email: email,
            email_war: !isValid,
            email_war_msg: "Please enter a valid email address"
        })
    }

    onChangePassword = (psw) => {
        this.checkPassword(psw, this.state.psw2)
    }

    onChangeConfirmPassword = (psw) => {
        this.checkPassword(this.state.psw1, psw)
    }

    checkPassword = (psw1, psw2) => {
        var regExp = /^(?=.*?[A-Z])(?=.*\d)(?=.*?[a-z])[a-zA-Z\d]{8,16}$/
        var isValid1 = regExp.test(psw1);
        var isValid2 = psw1 == psw2
        var psw1_war_msg = ""
        if (!isValid1) {
            psw1_war_msg = "Password is not following the correct format";
        }
        this.setState({
            psw1: psw1,
            psw2: psw2,
            psw1_war: !isValid1,
            psw1_war_msg: psw1_war_msg,
            psw2_war: !isValid2,
        })
    }

    signUpCall = () => {

        //api 호출
        var emaill = this.state.email;
        var psww = this.state.psw1;

        firebaseService.signUpUser(emaill, psww, this.onSuccess, this.onError)

    }

    onSuccess = () => {
        this.props.navigation.navigate('SignIn')
    }

    onError = (errorMessage) => {
        if (errorMessage == undefined) {
            errorMessage = "An error has occurred please try again";
        }
        console.log(errorMessage);
        this.setState({
            email_war: true,
            email_war_msg: errorMessage
        })
    }

    isSignUpValid = () => {
        if (this.state.email.length > 0 && this.state.psw1.length > 0 && this.state.email_war == false && this.state.psw1_war == false && this.state.psw2_war == false && this.state.radio == true) {
            console.log("Valid")
            return true;
        }
        console.log("InValid")
        return false;
    }

    render() {



        return (
            <View style={styles.contents_box}>
                <MyHeader title="Sign Up"
                    back={true}
                    emptyRight={true}
                    navigation={this.props.navigation} />

                <View style={styles.contents}>

                    <MainTextInput
                        placeholder="Email Address"
                        onChangeText={this.onChangeEmail}
                        keyboardType="email-address"
                        warning={this.state.email_war}
                        maxLength={320}
                        warning_msg={this.state.email_war_msg}
                        value={this.state.email} />

                    <MainTextInput
                        placeholder="Password"
                        value={this.state.psw1}
                        maxLength={16}
                        keyboardType="visible-password"
                        secureTextEntry={true}
                        warning={this.state.psw1_war}
                        warning_msg="Password is not following the correct format"
                        onChangeText={this.onChangePassword} />

                    <Text style={styles.psw_guide}>
                        Use 8 to 16 characters. Have at least one lowercase letter, one uppercase letter, and one number. Don't use your account name. Blank is not allowed. Special characters are not allowed.
                    </Text>

                    <MainTextInput
                        placeholder="ConfirmPassword"
                        value={this.state.psw2}
                        maxLength={16}
                        keyboardType="visible-password"
                        secureTextEntry={true}
                        warning={this.state.psw2_war}
                        warning_msg="Password is not the same"
                        onChangeText={this.onChangeConfirmPassword} />

                    <View>
                        <View style={styles.rbWrapper}>
                            <TouchableOpacity
                                style={styles.rrrrrr}
                                onPress={() => {
                                    this.setState({
                                        radio: !this.state.radio,
                                    });
                                }}>
                                {this.state.radio == false && <Image style={styles.radio} source={require('../../assets/img/rb_unchecked.png')} />}
                                {this.state.radio && <Image style={styles.radio} source={require('../../assets/img/rb_checked.png')} />}
                            </TouchableOpacity>
                            <Text style={styles.agree_guide}>I agree to the&nbsp;
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate('Terms')}
                                    style={styles.agree_btn}
                                >
                                    terms of Service
                                </TouchableOpacity>
                                &nbsp;and&nbsp;
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate('Terms')}
                                    style={styles.agree_btn}
                                >
                                    Data policy
                                </TouchableOpacity>.
                             </Text>
                        </View>
                    </View>


                    <MyButton
                        style={styles.btn_text}
                        title="SIGN UP" color={this.isSignUpValid() ? "#FFC424" : "#FFE49B"}
                        titleColor="#ffffff"
                        disabled={this.isSignUpValid() ? false : true}
                        onPress={() => this.signUpCall()}
                    />
                </View>




            </View>
        )
    }
}

const styles = StyleSheet.create({
    contents_box: {
        backgroundColor: '#ffffff',
        flex: 1,
    },
    psw_guide: {
        fontSize: 12,
        marginTop: 10,
        color: "#707070",
        marginStart: 10,
        marginEnd: 10,
    },
    agree_guide: {
        fontSize: 12,
        marginTop: 10,
        color: "#707070",
        marginStart: 8,
        marginEnd: 10,
    },
    contents: {
        padding: 24,
    },
    rbWrapper: {
        marginTop: 8,
        flexDirection: 'row',
        alignItems: 'center'
    },
    rbStyle: {
        height: 20,
        width: 20,
        borderRadius: 110,
        marginTop: 11,
        borderWidth: 1,
        borderColor: '#DBDBDB',
        alignItems: 'center',
        justifyContent: 'center',
    },
    selected: {
        width: 20,
        height: 20,
        borderRadius: 110,
        backgroundColor: '#FFC424',
        borderColor: 'none'
    },
    result: {
        marginTop: 22,
        color: 'white',
        fontWeight: '600',
        backgroundColor: 'blue',
    },
    btn_text: {
        fontWeight: 'bold',
    },
    agree_btn: {
        color: '#191919',
        textDecorationLine: 'underline',
        textDecorationColor: '#191919'

    }, radio: {
        width: 20,
        height: 20,
        marginTop: 11,
    }
})