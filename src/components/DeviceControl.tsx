import React from 'react';
import { View, Text, StyleSheet, Touchable } from 'react-native';
import MyHeader from '../ui/MyHeader';
import ColorBox from '../ui/ColorBox';
import MainTextInput from '../ui/MainTextInput';
import MyButton from '../ui/MyButton';
import ManualMode from './ManualMode';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { startAfter } from 'firebase/database/dist/database';

import firebaseService from '../firebase';
import AutoMode from './AutoMode';

export default class DeviceControl extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            color: 1,
            add: this.props.route.params.add,
            device_code: this.props.route.params.device_code,
            device_name: "SAMPLE DEVICE NAME",
            device_name_war: false,
            device_name_war_msg: "",
            device_mode: 0,
            height: undefined,
            angle: undefined,
            active: undefined,
        }
    }

    changeMode = () => {

        let newMode = this.state.device_mode == 0 ? 1 : 0
        this.setState({
            device_mode: newMode
        })

        firebaseService.changeMode(this.state.device_code, newMode)

    }

    componentDidMount() {
        firebaseService.deviceInfo(this.deviceInfo, this.state.device_code);
        firebaseService.getDeviceStatus(this.deviceStatus, this.state.device_code);
    }

    deviceStatus = (info) => {
        console.log("deviceStatus")
        console.log(info.val());
        let data = info.val();
        if (data == null || data.mode == null) return;
        if (data.mode == 0) {
            this.setState({
                device_mode: data.mode,
                height: parseInt(data.manualMode.height),
                angle: parseInt(data.manualMode.angle),
            })
        } else {
            this.setState({
                device_mode: data.mode,
                height: parseInt(data.autoMode.height),
                angle: parseInt(data.autoMode.angle),
                light: parseInt(data.autoMode.light),
                active: data.autoMode.activePreset,
            })
        }

    }

    deviceInfo = (info) => {

        let data = info.val();
        console.log("deviceInfo")
        if (data == null || data.regDate == null) return;
        console.log(data)
        this.setState({
            device_name: data.deviceName,
            color: data.color,
            reg_date: data.regDate,
        })
    }

    heightChanged = (h) => {
        h = parseInt(h);
        this.setState({
            height: h
        })
        if (this.state.device_mode == 0) {
            this.saveManualSetting(h, this.state.angle)
        }
    }

    angleChanged = (a) => {
        a = parseInt(a)
        this.setState({
            angle: a
        })
        if (this.state.device_mode == 0) {
            this.saveManualSetting(this.state.height, a)
        }
    }

    saveManualSetting = (h, a) => {
        firebaseService.saveManualSetting(h, a, this.state.device_code);
    }


    render() {
        return (
            <View style={styles.outer_box}>
                <MyHeader title={this.state.device_name}
                    back={true}
                    menu={true}
                    color={this.state.color}
                    email={this.props.route.params.email}
                    deviceCode={this.state.device_code}
                    navigation={this.props.navigation} />
                <TouchableOpacity style={styles.contents}
                    onPress={this.changeMode}>

                    <View style={styles.mode}>
                        <View style={[
                            styles.mode_text_box,
                            { backgroundColor: this.state.device_mode == 0 ? '#FFC424' : '#FFFFFF' }
                        ]}>
                            <Text style={[
                                styles.mode_text,
                                {
                                    color: this.state.device_mode == 0 ? '#ffffff' : '#999999',
                                    fontWeight: this.state.device_mode == 0 ? 'bold' : 'normal',
                                }
                            ]}>Manual</Text>
                        </View>
                        <View style={[
                            styles.mode_text_box,
                            { backgroundColor: this.state.device_mode == 1 ? '#FFC424' : '#FFFFFF' }
                        ]}>
                            <Text style={[
                                styles.mode_text,
                                {
                                    color: this.state.device_mode == 1 ? '#ffffff' : '#999999',
                                    fontWeight: this.state.device_mode == 1 ? 'bold' : 'normal',
                                }
                            ]}>Auto</Text>
                        </View>
                    </View>
                </TouchableOpacity>

                {(this.state.device_mode == 0 && this.state.height != undefined && this.state.angle != undefined) &&
                    < ManualMode
                        height={this.state.height}
                        angle={this.state.angle}
                        heightChanged={this.heightChanged}
                        angleChanged={this.angleChanged}
                    />
                }

                {this.state.device_mode == 1 &&
                    <AutoMode
                        height={this.state.height}
                        angle={this.state.angle}
                        device_code={this.state.device_code}
                        light={this.state.light}
                        navigation={this.props.navigation}
                        active={this.state.active}
                        onPreset={(add) => {
                            this.props.navigation.navigate('AutoPreset', {
                                add: add,
                                device_code: this.state.device_code,
                            })
                        }} />
                }
            </View>
        )
    }

}

const styles = StyleSheet.create({
    contents: {
        padding: 24,
        backgroundColor: '#ffffff',
        flex: 1,
    }, outer_box: {
        backgroundColor: '#ffffff',
        flex: 1,
    }, mode: {
        borderColor: '#DBDBDB',
        borderWidth: 0.5,
        borderRadius: 9,
        width: 154,
        padding: 2,
        alignContent: 'center',
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',

    }, mode_text_box: {
        width: 75,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 7.5,
    }, mode_text: {
        alignContent: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        fontSize: 12,
    }, manual_chk: {

    }
})