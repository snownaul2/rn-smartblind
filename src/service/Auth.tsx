import React, { useState } from 'react';
import { authService, firebaseInstance } from '../firebase';

const Auth = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [newAccount, setNewAccount] = useState(true);
  const [error, setError] = useState("");
  const onChange = (event) => {
    const {target: {name, value}} = event;
    if(name === "email"){
      setEmail(value)
    } else if (name === "password"){
      setPassword(value)
    }
  }

  const onSubmit = async (event) => {
    event.preventDefault();
    try {
      let data;
      if (newAccount) {
        // create account
        data = await authService.createUserWithEmailAndPassword(email, password);
        console.log('sign up')
      } else {
        // log in
        data = await authService.signInWithEmailAndPassword(email, password);
        console.log('login')
      }
      console.log(data)
    } catch(error){
      console.log(error);
      setError(error.message)
    }
  }

  const toggleAccount = () => setNewAccount((prev) => !prev);
  const onSocialClick = (event) => {
    console.log(event.target);
  }
};

export default Auth;
