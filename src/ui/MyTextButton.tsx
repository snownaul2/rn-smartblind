import React from "react";
import { SafeAreaView, StyleSheet, Text, Image } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
// import Style from '../Styles';

export default class MyTextButton extends React.Component {
    // const [text, onChangeText] = React.useState("Useless Text");
    // const [number, onChangeNumber] = React.useState(null);

    render() {


        return (
            <TouchableOpacity style={[
                styles.button,
            ]}
                onPress={this.props.onPress}>
                <Text style={[
                    styles.title,
                    { color: this.props.titleColor }
                ]}>{this.props.title}</Text>
            </TouchableOpacity>
        );
    }
};

const styles = StyleSheet.create({
    button: {
        marginTop: 40,
        justifyContent: 'center',
        alignItems: 'center',
        fontWeight: "bold",
        fontSize: 36,
    },
    title: {
        fontSize: 16,
        alignSelf: "center",
    },

});
