import React, { Component } from 'react';
import {
    Animated,
    PanResponder,
    StyleSheet,
    Text,
    View,
} from 'react-native';

export default class AutoSlider extends Component {
    static defaultProps = {
        value: 40,
        maximumValue: 100,
        minimumValue: 0,
        onSlidingStart: () => { },
        onValueChange: () => { },
        onSlidingComplete: () => { },
    }

    constructor(props) {
        super()
        this.state = {
            value: props.value,
        }

        this.range = props.maximumValue - props.minimumValue
    }

    componentWillMount() {
        // this.panResponder = PanResponder.create({
        //     onStartShouldSetPanResponder: (e, gestureState) => true,
        //     onPanResponderGrant: (evt, gestureState) => {
        //         this.props.onSlidingStart()
        //         this.setState({ anchorValue: this.state.value })
        //     },
        //     onPanResponderMove: Animated.event([null, {}], { listener: this.handleSlide }),
        //     onPanResponderRelease: (evt, gestureState) => {
        //         this.props.onSlidingComplete()
        //     },
        // })
    }

    slideTo = (value) => {
        this.setState({ value })
    }

    onLayout = ({ nativeEvent }) => {
        this.setState({
            width: nativeEvent.layout.width,
            height: nativeEvent.layout.height,
        })
    }

    // handleSlide = (evt, gestureState) => {
    //     const { maximumValue, minimumValue } = this.props
    //     let valueIncrement = (-gestureState.dy * this.range) / this.state.height
    //     let nextValue = this.state.anchorValue + valueIncrement
    //     nextValue = nextValue >= maximumValue ? maximumValue : nextValue
    //     nextValue = nextValue <= minimumValue ? minimumValue : nextValue

    //     this.props.onValueChange(nextValue)
    //     console.log(nextValue);
    //     this.props.percentage(nextValue);
    //     this.setState({ value: nextValue })
    // }

    render() {
        const value = this.props.value
        const unitValue = (value - this.props.minimumValue) / this.range

        return (
            <View
                style={styles.autoSlider}
            >
                <View>
                    <Text style={styles.percentage}>100%</Text>
                    <Text style={styles.percentage}>75%</Text>
                    <Text style={styles.percentage}>50%</Text>
                    <Text style={styles.percentage}>25%</Text>
                    <Text style={styles.percentage}>0%</Text>
                </View>
                <View
                    onLayout={this.onLayout}
                    style={[styles.container, this.props.style]}
                // {...this.panResponder.panHandlers}
                >
                    <View style={[styles.pendingTrack, { flex: 1 - unitValue }]} />
                    <View style={[styles.track, { flex: unitValue }, this.props.trackStyle]}>
                        <View style={styles.dash_box}>
                            <View style={styles.thumb} />
                            <View style={styles.thumb} />
                            <View style={styles.thumb} />
                            <View style={styles.thumb} />
                            <View style={styles.thumb} />
                        </View>

                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    autoSlider: {
        flex: 0,
        flexDirection: 'row',
        marginRight: 200,
    }, container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F1F1F5',
        borderRadius: 12,
        overflow: 'hidden',
        width: 100,
        minWidth: 100,
        minHeight: 200,
        maxHeight: 200,
        maxWidth: 100,
    },
    pendingTrack: {
    },
    track: {
        flex: 1,
        backgroundColor: '#FFC424',
        borderRadius: 12,
        alignSelf: 'stretch',
    },
    trackLabel: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    trackLabelText: {
        color: 'white',
        fontWeight: '600',
    },
    thumb: {
        backgroundColor: '#000000',
        borderRadius: 12,
        height: 3,
        width: 15,
        marginHorizontal: 3,
        alignSelf: 'center',
    }, percentage: {
        color: '#999999',
        fontSize: 12,
        marginBottom: 30,
        marginRight: 10,
    }, dash_box: {
        flexDirection: 'row',
        justifyContent: 'center',
    }
})

function formatNumber(x) {
    return x.toFixed(1).replace(/\.?0*$/, '')
}