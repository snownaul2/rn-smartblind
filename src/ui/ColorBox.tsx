import React from "react";
import { View, StyleSheet, Image } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
// import Style from '../Styles';

export default class MyButton extends React.Component {
    // const [text, onChangeText] = React.useState("Useless Text");
    // const [number, onChangeNumber] = React.useState(null);

    // componentDidUpdate(){
    //     if(this.props.disabled){

    //     }
    // }

    render() {

        return (
            <View
                style={styles.box}>
                <TouchableOpacity style={[
                    styles.button,
                    { backgroundColor: this.props.color }
                ]}
                    onPress={() => this.props.onPress(1)}
                    disabled={this.props.disabled}>
                    {this.props.color != 1 &&
                        <Image
                            style={styles.color}
                            source={require('../../assets/img/redx.png')} />
                    }
                    {this.props.color == 1 &&
                        <Image
                            style={styles.color}
                            source={require('../../assets/img/red.png')} />
                    }
                </TouchableOpacity>

                <TouchableOpacity style={[
                    styles.button,
                    { backgroundColor: this.props.color }
                ]}
                    onPress={() => this.props.onPress(2)}
                    disabled={this.props.disabled}>
                    {this.props.color != 2 &&
                        <Image
                            style={styles.color}
                            source={require('../../assets/img/yellowx.png')} />
                    }
                    {this.props.color == 2 &&
                        <Image
                            style={styles.color}
                            source={require('../../assets/img/yellow.png')} />
                    }
                </TouchableOpacity>
                <TouchableOpacity style={[
                    styles.button,
                    { backgroundColor: this.props.color }
                ]}
                    onPress={() => this.props.onPress(3)}
                    disabled={this.props.disabled}>
                    {this.props.color != 3 &&
                        <Image
                            style={styles.color}
                            source={require('../../assets/img/greenx.png')} />
                    }
                    {this.props.color == 3 &&
                        <Image
                            style={styles.color}
                            source={require('../../assets/img/green.png')} />
                    }
                </TouchableOpacity>
                <TouchableOpacity style={[
                    styles.button,
                    { backgroundColor: this.props.color }
                ]}
                    onPress={() => this.props.onPress(4)}
                    disabled={this.props.disabled}>
                    {this.props.color != 4 &&
                        <Image
                            style={styles.color}
                            source={require('../../assets/img/bluex.png')} />
                    }
                    {this.props.color == 4 &&
                        <Image
                            style={styles.color}
                            source={require('../../assets/img/blue.png')} />
                    }
                </TouchableOpacity>
                <TouchableOpacity style={[
                    styles.button,
                    { backgroundColor: this.props.color }
                ]}
                    onPress={() => this.props.onPress(5)}
                    disabled={this.props.disabled}>
                    {this.props.color != 5 &&
                        <Image
                            style={styles.color}
                            source={require('../../assets/img/purplex.png')} />
                    }
                    {this.props.color == 5 &&
                        <Image
                            style={styles.color}
                            source={require('../../assets/img/purple.png')} />
                    }
                </TouchableOpacity>
                <TouchableOpacity style={[
                    styles.button,
                    { backgroundColor: this.props.color }
                ]}
                    onPress={() => this.props.onPress(6)}
                    disabled={this.props.disabled}>
                    {this.props.color != 6 &&
                        <Image
                            style={styles.color}
                            source={require('../../assets/img/blackx.png')} />
                    }
                    {this.props.color == 6 &&
                        <Image
                            style={styles.color}
                            source={require('../../assets/img/black.png')} />
                    }
                </TouchableOpacity>
            </View>

        );
    }
};

const styles = StyleSheet.create({
    button: {
        height: 52,

        justifyContent: 'center',
        alignItems: 'center',
        fontWeight: "bold",
        fontSize: 36,
    },
    color: {
        height: 30,
        width: 30,
        marginHorizontal: 8,
    },
    box: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 20,
        marginBottom: 20,
    }
});
