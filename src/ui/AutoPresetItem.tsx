import React from "react";
import { Dimensions, View, StyleSheet, Text, Image } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import firebaseService from '../firebase'
// import Style from '../Styles';

export default class AutoPreset extends React.Component {
    // const [text, onChangeText] = React.useState("Useless Text");
    // const [number, onChangeNumber] = React.useState(null);

    // componentDidUpdate(){
    //     if(this.props.disabled){

    //     }
    // }

    constructor(props) {
        super(props);
        this.state = {
            height: undefined,
            angle: undefined,
            light: undefined,
        }

    }





    render() {

        console.log(this.props.preset_code, this.props.active);

        return (
            <View style={styles.outer_box}>

                <Text style={this.props.active == 1 ? styles.activated : styles.unactivated}>{this.props.light}%</Text>

                <View style={styles.ha_inner_box}>
                    <Text style={styles.s_title}>
                        Height
                        </Text>
                    <View style={styles.prc_box}>
                        <Text style={styles.num}>{this.props.height}%</Text>
                    </View>
                </View>

                <View style={styles.ha_inner_box}>
                    <Text style={styles.s_title}>
                        Angle
                        </Text>
                    <View style={styles.prc_box}>
                        <Text style={styles.num}>{this.props.angle}%</Text>
                    </View>
                </View>

                <TouchableOpacity
                    style={styles.on_off_box}
                    onPress={() => this.props.onOffChanged(this.props.on)}
                >
                    {this.props.on == 1 &&
                        <Image
                            style={styles.onoff}
                            source={require('../../assets/img/on.png')} />

                    }
                    {
                        this.props.on == 0 &&
                        <Image
                            style={styles.onoff}
                            source={require('../../assets/img/off.png')} />
                    }
                </TouchableOpacity>

                <TouchableOpacity
                    style={styles.on_off_box}
                    onPress={() => this.props.menu(this.props.key)}
                >
                    <Image
                        style={styles.menu}
                        source={require('../../assets/img/preset_menu.png')} />
                </TouchableOpacity>

            </View>

        );
    }
};

const styles = StyleSheet.create({

    outer_box: {
        height: 64,
        paddingHorizontal: 24,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: 'white',
    }, activated: {
        height: 33,
        width: 55,
        fontSize: 14,
        color: '#FFFFFF',
        backgroundColor: '#FFC424',
        alignContent: 'center',
        borderRadius: 9,
        justifyContent: 'center',
        alignItems: 'center',
        textAlignVertical: "center",
        textAlign: 'center',
        fontWeight: 'bold',
        paddingTop: 7,
        marginRight: 24,
        marginTop: 14,
        marginLeft: 15,
    }, unactivated: {
        height: 33,
        width: 55,
        fontSize: 14,
        color: '#191919',
        borderRadius: 9,
        borderColor: '#DBDBDB',
        borderWidth: 0.5,
        textAlignVertical: "center",
        textAlign: 'center',
        paddingTop: 7,
        marginRight: 24,
        marginTop: 14,
        marginLeft: 15,
    }, ha_inner_box: {
        marginHorizontal: 24,
        width: 50,
        alignContent: 'center',
        marginTop: 12.5,

    }, s_title: {
        fontSize: 12,
        color: '#999999',

    }, num: {
        fontSize: 14,
        color: '#191919'
    }, onoff: {
        width: 35,
        height: 20,
    }, on_off_box: {
        marginHorizontal: 12,
        marginTop: 19,

    }, menu: {
        width: 28,
        height: 20,
        marginTop: 3,
        marginLeft: 10
    }

});
