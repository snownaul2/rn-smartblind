import React from "react";
import { Dimensions, View, StyleSheet, Text, Image } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import firebaseService from '../firebase'
// import Style from '../Styles';

export default class MyDeviceItem extends React.Component {
    // const [text, onChangeText] = React.useState("Useless Text");
    // const [number, onChangeNumber] = React.useState(null);

    // componentDidUpdate(){
    //     if(this.props.disabled){

    //     }
    // }

    constructor(props) {
        super(props);
        this.state = {
            device_mode: 0,
            height: undefined,
            angle: undefined,
        }

        this.deviceStatus.bind(this);
    }

    componentDidMount() {
        if (!this.props.add)
            firebaseService.getDeviceStatus(this.deviceStatus, this.props.deviceCode);
    }

    deviceStatus = (info) => {
        console.log("deviceStatus")
        console.log(info.val());
        let data = info.val();
        // if (data.ManualMode) {
        // console.log('here')
        if (data == null || data.mode == null) return;
        console.log("mode : " + data.mode);
        if (data.mode == 0) {
            this.setState({
                device_mode: parseInt(data.mode),
                height: parseInt(data.manualMode.height),
                angle: parseInt(data.manualMode.angle),
            })
        } else {
            this.setState({
                device_mode: parseInt(data.mode),
                height: parseInt(data.autoMode.height),
                angle: parseInt(data.autoMode.angle)
            })
        }
        // }
    }



    render() {

        const chartWidth = (Dimensions.get('window').width - 48) / 2;
        console.log(chartWidth)

        var bgColor;
        if (this.props.color) {
            switch (this.props.color) {
                case 1:
                    bgColor = "#FC79A6"
                    break;
                case 2:
                    bgColor = "#FFC424"
                    break;
                case 3:
                    bgColor = "#5BD93D"
                    break;
                case 4:
                    bgColor = "#3DD9D9"
                    break;
                case 5:
                    bgColor = "#9F62E5"
                    break;
                default:
                    bgColor = "#5D5D5D"

            }
        }

        return (
            <View style={styles.custom_margin}>
                <View style={styles.outer_box}>

                    {
                        this.props.add &&
                        <TouchableOpacity
                            style={[
                                styles.button,
                                // { width: chartWidth }
                            ]}
                            onPress={this.props.onPress}
                            disabled={this.props.disabled}>
                            <Image
                                style={styles.add}
                                source={require('../../assets/img/add.png')} />
                        </TouchableOpacity>
                    }
                    {
                        this.props.deviceCode &&
                        <TouchableOpacity
                            onPress={this.props.onPress}
                        >
                            <View style={styles.contents}>
                                <View style={[
                                    styles.color_box,
                                    { backgroundColor: bgColor }
                                ]}>
                                    <Image
                                        style={styles.alarm}
                                        source={require('../../assets/img/alarm_on.png')} />
                                    <Text style={[
                                        styles.title,
                                    ]}>{this.props.deviceName}</Text>
                                </View>

                                <View style={styles.info_box}>
                                    <View style={styles.flex} />
                                    {this.state.device_mode == 1 &&
                                        <Image
                                            style={styles.mode}
                                            source={require('../../assets/img/auto.png')} />
                                    }
                                    {this.state.device_mode == 0 &&
                                        <Image
                                            style={styles.mode}
                                            source={require('../../assets/img/manual.png')} />
                                    }
                                    <View style={styles.flex} />
                                    <View style={styles.num_box}>
                                        <Text style={styles.s_title}>Height</Text>
                                        <View style={styles.number_box}>
                                            <Text style={styles.info}>{this.state.height}</Text>
                                            <Text style={styles.info}>%</Text>
                                        </View>
                                    </View>
                                    <View style={styles.flex} />
                                    <View style={styles.num_box}>
                                        <Text style={styles.s_title}>Angle</Text>
                                        <View style={styles.number_box}>
                                            <Text style={styles.info}>{this.state.angle}</Text>
                                            <Text style={styles.info}>%</Text>
                                        </View>
                                    </View>
                                    <View style={styles.flex} />
                                </View>
                            </View>
                        </TouchableOpacity>
                    }

                </View>
            </View>

        );
    }
};

const styles = StyleSheet.create({
    custom_margin: {
        height: 120,
        paddingHorizontal: 12,
        width: '50%',
        marginBottom: 24,

    },
    outer_box: {
        height: 120,
        marginTop: 20,
        backgroundColor: '#ffffff',
        width: '100%',
    },
    button: {
        height: 120,
        justifyContent: 'center',
        alignItems: 'center',
        fontWeight: "bold",
        fontSize: 36,

    },
    contents: {
        flexDirection: 'column'
    },
    color_box: {
        height: 75,
        // flex: 1,
    },
    title: {
        fontSize: 16,
        color: "#ffffff",
        flex: 1,
        marginLeft: 12,
        marginTop: 12,
        justifyContent: 'flex-end',
        fontWeight: "bold"
    },
    add: {
        height: 28,
        width: 28,
    },
    mode: {
        height: 28,
        width: 28,
        alignSelf: 'center'
    },
    alarm: {
        alignSelf: "flex-end",
        height: 28,
        width: 28,
        marginTop: 7,
        marginRight: 7,
    },
    info_box: {
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'center',
        height: 45,
    },
    num_box: {
        alignSelf: 'center',
    },
    number_box: {
        flexDirection: 'row',
    },
    s_title: {
        fontSize: 10,
        color: "#999999"
    },
    info: {
        fontSize: 12,
        color: "#191919",
        fontWeight: 'bold'
    },
    flex: {
        flex: 1,
    }
});
