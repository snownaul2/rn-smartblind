import React from "react";
import { View, Image, StyleSheet, Text } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";

export default class MyMenuItem extends React.Component {

    render() {
        return (
            <View>
                <View style={styles.header_box}>
                    <TouchableOpacity style={styles.title_box}
                        onPress={this.props.onPress}>
                        <Text style={styles.title}>{this.props.title}</Text>
                    </TouchableOpacity>

                </View>
                <View style={styles.line}></View>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    header_box: {
        height: 52,
        alignItems: 'center',
        paddingStart: 24,
        paddingEnd: 24,
        flexDirection: 'row'
    },
    title: {
        fontSize: 14,
        color: "#191919",
    },
    line: {
        height: 0.5,
        flex: 1,
        width: 100,
        backgroundColor: "#EDEDED"
    }

});
