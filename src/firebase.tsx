import { initializeApp } from 'firebase/app';
import { getDatabase, ref, set, onValue, child, get, push, update, orderByKey, orderByChild, query, onChildAdded, onChildChanged, onChildRemoved, DataSnapshot } from 'firebase/database';
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut, onAuthStateChanged } from "firebase/auth";

// Initialize Firebase
const firebaseConfig = {
  apiKey: "AIzaSyDrXKJMaYyJHrodN_RRqQGCcAhzv5zEEDA",
  authDomain: "smart-blind-ktj.firebaseapp.com",
  databaseURL: "https://smart-blind-ktj-default-rtdb.firebaseio.com",
  projectId: "smart-blind-ktj",
  storageBucket: "smart-blind-ktj.appspot.com",
  messagingSenderId: "140404078254",
  appId: "1:140404078254:web:188acdcd30026f88309f3f",
  measurementId: "G-Y19RYCZ2MB"
};

const app = initializeApp(firebaseConfig);

//Get a reference to the database service
const database = getDatabase(app);

class FirebaseService {
  // Sign up
  signUpUser(email: string, password: string, onSuccess: any, onError: any) {
    const auth = getAuth();
    createUserWithEmailAndPassword(auth, email, password).then((userCredential) => {
      // Signed in
      const user = userCredential.user;

      set(ref(database, 'users/' + user.uid), {
        email: user.email
      });

      onSuccess()
    })
      .catch((error) => {
        var errorCode;
        var errorMessage = "An error has occurred please try again";

        if (error != undefined) {
          errorMessage = error.message;
        }
        onError(errorMessage)

        return error.code + ", " + error.message;
        // console.log(error.code + ", " + error.message);

        // ..
      });
  }

  // Sign in
  signInUser(email: string, password: string, onSuccess: any, onError: any) {
    const auth = getAuth();
    signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        // Signed in
        var user = userCredential.user;

        onSuccess()

      })
      .catch((error) => {
        var errorCode = error.code;
        var errorMessage = error.message;
      });
  }

  // Sign out
  signOutUser(onSuccess: any) {
    const auth = getAuth();
    signOut(auth).then(() => {
      // Sign-out successful.
      console.log('sign out successfully');
      onSuccess();
    }).catch((error) => {
      // An error happened.
      console.log('sign out failed');
    });
  }

  // register device
  registerDevice(deviceCode: string, deviceName: string, deviceColor: string) {
    // find the user's uid
    const auth = getAuth();
    onAuthStateChanged(auth, (user) => {
      if (user) {
        // User is signed in, see docs for a list of available properties
        // https://firebase.google.com/docs/reference/js/firebase.User
        const uid = user.uid;

        var dateTime = this.getDateTime();

        // store in users
        set(ref(database, `users/${uid}/devices/${deviceCode}`), {
          color: deviceColor,
          regDate: dateTime,
          deviceName: deviceName
        });

        // store in devices
        set(ref(database, 'devices/' + deviceCode + '/manualMode/'), {
          angle: 0,
          height: 0
        });

        set(ref(database, 'devices/' + deviceCode + '/autoMode/'), {
          activePreset: '',
          angle: 0,
          height: 0,
          light: 0  // current light
        });

        // default1 preset
        push(ref(database, 'devices/' + deviceCode + '/autoMode/preset/'), {
          angle: 50,
          height: 50,
          preLight: 80,
          button: 1  // on, off
          //active: 0   // 활성화, 비활성화
        });

        // default2 preset
        push(ref(database, 'devices/' + deviceCode + '/autoMode/preset/'), {
          angle: 30,
          height: 30,
          preLight: 50,
          button: 1  // on, off
          //active: 0   // 활성화, 비활성화
        });

        // default3 preset
        push(ref(database, 'devices/' + deviceCode + '/autoMode/preset/'), {
          angle: 10,
          height: 10,
          preLight: 30,
          button: 1  // on, off
          //active: 0   // 활성화, 비활성화
        });

        // set mode
        update(ref(database, `devices/${deviceCode}`), {
          'mode': 0
        });

      } else {
        // User is signed out
        // ...

        console.log('Login pleaseeee')
      }
    });
  }
  
  // edit device info
  editDeviceInfo(deviceCode: string, deviceName: string, deviceColor: string){
    // find the user's uid
    const auth = getAuth();
    onAuthStateChanged(auth, (user) => {
      if (user) {
        // User is signed in, see docs for a list of available properties
        // https://firebase.google.com/docs/reference/js/firebase.User
        const uid = user.uid;

        // update info
        update(ref(database, `users/${uid}/devices/${deviceCode}`), {
          color: deviceColor,
          deviceName: deviceName
        });
      } else {
        // User is signed out
        // ...
        console.log('Login pleaseeee')
      }
    });
  }

  // show device list
  deviceList(change: any) {
    const auth = getAuth();
    const userId = auth.currentUser?.uid;

    // call deviceList Query
    const deviceListQuery = query(ref(database, `users/${userId}/devices/`), orderByChild('regDate'));
    return onValue(deviceListQuery, change);
  }

  // show device info
  deviceInfo(change: any, deviceCode: any) {
    const auth = getAuth();
    const userId = auth.currentUser?.uid;

    return onValue(ref(database, 'users/' + userId + '/devices/' + deviceCode), change);
  }

  // get date and time
  getDateTime() {
    var dt = new Date();
    var day = ("0" + dt.getDate()).slice(-2);
    var month = ("0" + (dt.getMonth() + 1)).slice(-2);
    var year = dt.getFullYear();

    var date = year + "-" + month + "-" + day;

    var hours = dt.getHours();
    var minutes = dt.getMinutes();
    var seconds = dt.getSeconds();

    var dateTime = year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;

    return dateTime;
  }

  // device status (name, height, angle, mode)
  getDeviceStatus(change: any, deviceCode: any) {
    // update active preset (refresh)
    this.updateActivePreset(change, deviceCode);

    // testing
    onChildChanged(ref(database, `devices/${deviceCode}/`), (data) => {
      console.log('!!!!!!!!!!CHANGED!!!!!!')
       // update active preset (refresh)
      this.updateActivePreset(change, deviceCode);
      //setCommentValues(postElement, data.key, data.val().text, data.val().author);
    });

    return onValue(ref(database, `devices/${deviceCode}/`), change) 
  }

  // remove a device
  removeDevcie(deviceCode: any){
    const auth = getAuth();
    const userId = auth.currentUser?.uid;

    // remove from 'devices'
    set(ref(database, `devices/${deviceCode}`), {
      autoMode: null
    });

    // remove from 'users'
    set(ref(database, `users/${userId}/devices/${deviceCode}`), {
      autoMode: null
    });

  }

  // update active preset key
  updateActivePreset(change: any, deviceCode: any){
    return onValue(ref(database, `devices/${deviceCode}/`), (snapshot) => {
      var curActivePreset:any = ""
      //console.log('[SNAP] ' , snapshot.val().mode);
      if (snapshot.exists()){        
        // get current light
        var light = this.getLight(deviceCode)
        //var light = 90 // testing
        
        console.log('[light] ' , light)
        
        var maxLight = 0
        var minLight = 100
    
        var height = 0
        var angle = 0
        
        // check if the button is all off
        var isAllOff = false;
        var offCnt = 0
        var listCnt = 0
        
        // preset check
        const dbRef = ref(database, `devices/${deviceCode}/autoMode/preset/`);
        
        onChildAdded(dbRef, (data) => {     
          // get each preset values
          var presetKey = data.key;
          var presetData = data.val();
          var presetLight = presetData.preLight;
          var presetHeight = presetData.height;
          var presetAngle = presetData.angle;
          var presetButton = presetData.button;

          if (presetButton == 1){
            // get max and min
            if (light >= presetLight){
              if (maxLight < presetLight){
                maxLight = presetLight
                curActivePreset = presetKey
                height = presetHeight;
                angle = presetAngle;
              }
            } else {
              if (maxLight == 0){
                if (minLight > presetLight){
                  minLight = presetLight
                  curActivePreset = presetKey
                  height = presetHeight;
                  angle = presetAngle;
                }
              }
            }

          } else {
            // button-off check button
            offCnt++
          }
          listCnt++

          // console.log('cnt|offCnt ', listCnt, '|', offCnt)
          // // buttons are all OFF -> last preset button is UNABLE to turn OFF
          // if (offCnt == listCnt){
          //   console.log('PRESET: ' , presetKey)
          //   update(ref(database, `devices/${deviceCode}/autoMode/preset/${presetKey}`), {
          //     button: 1
          //   });
          // }
        });

        // update in automode 
        update(ref(database, `devices/${deviceCode}/autoMode/`), {
          activePreset: curActivePreset,
          height: height,
          angle: angle
        });
      }      
    }, {
      onlyOnce: true
    });
  }

  // save manual mode setting
  saveManualSetting(height: any, angle: any, deviceCode: any) {
    const auth = getAuth();
    const userId = auth.currentUser?.uid;

    // store manual setting in database
    update(ref(database, `devices/${deviceCode}/manualMode/`), {
      height: height,
      angle: angle
    });
  }

  // change mode (0: Manual, 1: Auto)
  changeMode(deviceCode: any, deviceMode: any) {
    update(ref(database, 'devices/' + deviceCode), {
      'mode': deviceMode
    });
  }

  // preset status - Auto mode
  getPresetStatus(change: any, deviceCode: any) {
    return onValue(ref(database, `devices/${deviceCode}`), change);
  }

  // cnt of button Off prsets - Auto mode
  getOffPresetCnt(deviceCode: any, presetKey: any){
    const dbRef = ref(database, `devices/${deviceCode}/autoMode/preset/${presetKey}`)
    return onValue(dbRef, (snapshot) => {
      var pListCnt = snapshot.size;
      console.log('[PLISTCNT] ' , pListCnt)
    }, {
      onlyOnce: true
    });
  }

  // get current light
  getLight(deviceCode: any){    
    let childLight = 0

    onValue(ref(database, `devices/${deviceCode}/autoMode/light`), (snapshot) => {
      childLight = snapshot.val();
    }, {
      onlyOnce: true
    });

    return childLight
  }

  // preset list - Auto mode
  presetList(change: any, deviceCode: any){   

    // update active preset (refresh)
    this.updateActivePreset(deviceCode)

    // call presetList Query
    const presetListQuery = query(ref(database, `devices/${deviceCode}/autoMode/preset/`), orderByChild('preLight'));
    console.log('[pList] ' , presetListQuery)
    return onValue(presetListQuery, (info) => {
      this.updateActivePreset(deviceCode);

      change(info);
    });
  }

  // save a preset - Auto mode
  savePreset(change: any, deviceCode: any, height: any, angle: any, userLight: any) {
    
    // Get a key for a new preset.
    const newPostKey = push(child(ref(database), 'posts')).key;

    // create a preset
    const presetValue = {
      angle: angle,
      height: height,
      preLight: userLight,
      button: 1  // on, off
    };

    // Write the new post's data simultaneously in the posts list and the user's post list.
    const updates = {};
    updates[`devices/${deviceCode}/autoMode/preset/${newPostKey}`] = presetValue;

    return update(ref(database), updates);
  }

  // update a preset - Auto mode
  updatePreset(change: any, deviceCode: any, height: any, angle: any, preLight: any, button: any, presetKey: any) {
    //var curActive = this.getActiveStatus(light, preLight)
    
    // update and store a preset in the database
    update(ref(database, `devices/${deviceCode}/autoMode/preset/${presetKey}/`), {
      angle: angle,
      height: height,
      preLight: preLight,
      button: button  // on, off
    });
  }

  // remove a preset - Auto mode
  removePreset(deviceCode: any, presetKey: any){
    // remove a preset
    set(ref(database, `devices/${deviceCode}/autoMode/preset/${presetKey}/`), {
      angle: null,
      height: null,
      preLight: null,
      button: null  // on, off
    });
  }
  

}

export default new FirebaseService();