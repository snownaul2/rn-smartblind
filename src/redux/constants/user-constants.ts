import { statusEnum } from ".";

export const SIGNIN = 'SIGNIN';

export interface SignInAction {
    type: typeof SIGNIN;
    status: statusEnum;
    skipToReducer: boolean;
    payload: {
        email: string;
        psw: string;
    }
}