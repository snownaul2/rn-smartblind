import { Saga } from 'redux-saga'
import { AnyAction } from 'redux'
import { statusEnum } from '../constants'
import { SIGNIN } from '../constants/user-constants';

function wrapSaga(saga: Saga) {
    return (action: AnyAction): any => {
        if (action.status !== statusEnum.PENDING || action.skipToReducer) {
            return null;
        }
        return saga(action);
    }
}

// export default funciton* (): any {
//     yield all([
//         yield takeLatest(SIGNIN, wrapSaga());
//     ])
// }