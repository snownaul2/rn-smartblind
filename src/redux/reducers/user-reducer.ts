import { UserResource } from '../state';
import { statusEnum } from '../constants';
import { AnyAction } from 'redux';
import { SIGNIN } from '../constants/user-constants'
const initialUser: UserResource = {
    data: {
        id: '',
        email: '',
    },
    status: statusEnum.PENDING,
}

export function getUserReducer(
    state = initialUser,
    action: AnyAction
): UserResource {
    switch (action.type) {
        case SIGNIN:
            return { data: action.payload, status: action.status };
        default:
            return state;
    }
}