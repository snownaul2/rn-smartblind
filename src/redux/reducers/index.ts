import { AppState } from "../state";
import { combineReducers, AnyAction, Reducer } from "redux";
import { RootStateOrAny } from "react-redux";
import { statusEnum, NONE } from "../constants";
import * as userReducer from './user-reducer';

function wrapReducerRoot(reducer: Reducer) {
    return (state: RootStateOrAny, action: AnyAction): any => {
        if (action.skipToReducer) {
            action.status = statusEnum.SUCCESS;
        } else if (action.status === statusEnum.ERROR) {
            return reducer(state, action);
        } else if (action.status !== statusEnum.SUCCESS) {
            return reducer(state, { type: NONE });
        }
        return reducer(state, action);
    }
}

export const createRootReducer = (): Reducer<AppState> =>
    combineReducers<AppState>({
        user: wrapReducerRoot(userReducer.getUserReducer),
    })