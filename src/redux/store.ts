import { createBrowserHistory } from 'history';
import * as Redux from 'redux';
import { createRootReducer } from './reducers';


const store = Redux.createStore(
    createRootReducer(),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

export { store };