import { SignInAction, SIGNIN } from "../constants/user-constants";
import { statusEnum } from "../constants";


export function signIn(email: string, psw: string): SignInAction {
    return {
        type: SIGNIN,
        status: statusEnum.PENDING,
        skipToReducer: false,
        payload: {
            email: email,
            psw: psw
        }
    }
}