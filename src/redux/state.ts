export interface AppState {
    user: UserResource;
}

export interface User {
    id: string;
    email: string;
}


export interface UserResource {
    data: User;
    status: string;
}

