import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from '@react-navigation/native';
import Splash from './components/Splash';
import SignIn from './components/SignIn';
import SignUp from './components/SignUp';
import Devices from './components/Devices';
import Terms from './components/settings/Terms';
import Settings from './components/settings/Settings';
import DeviceAdd from './components/DeviceInfo';
import DeviceInfo from './components/DeviceInfo';
import DeviceControl from './components/DeviceControl';
import AutoPreset from './components/AutoPreset';

const Stack = createStackNavigator()



const Routes: React.FC = () => {

    return (
        <NavigationContainer>
            <Stack.Navigator>
                {/* <Stack.Screen name="Splash" component={Splash} /> */}
                <Stack.Screen name="SignIn" component={SignIn} options={{ headerShown: false }} />
                <Stack.Screen name="SignUp" component={SignUp} options={{ headerShown: false }} />
                {/* <Stack.Screen name="SignIn" component={SignIn} options={{ headerShown: false }} /> */}


                <Stack.Screen name="Devices" component={Devices} options={{ headerShown: false }} />
                <Stack.Screen name="DeviceInfo" component={DeviceInfo} options={{ headerShown: false }} />
                <Stack.Screen name="DeviceControl" component={DeviceControl} options={{ headerShown: false }} />
                <Stack.Screen name="AutoPreset" component={AutoPreset} options={{ headerShown: false }} />
                <Stack.Screen name="Terms" component={Terms} options={{ headerShown: false }} />
                <Stack.Screen name="Settings" component={Settings} options={{ headerShown: false }} />

            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default Routes;